Pod::Spec.new do |spec|
  spec.name         = 'iLA'
  spec.version      = '1.3.0'
  spec.platform     = :ios, '5.0'
  spec.license      = 'private'
  spec.source       = { :git => 'https://bitbucket.org/znafets/ila.git', :tag => spec.version.to_s }
  spec.source_files = 'src/*.{h,m,c}'

  spec.dependency 'DTRichTextEditor', '1.5.1'
  spec.dependency 'ShareKit', '~> 2.3.1'
  spec.dependency 'AFNetworking', '~> 1.3.1'
  spec.dependency 'CocoaAsyncSocket', '~> 7.3.1'
  spec.dependency 'THObserversAndBinders', '~> 1.0.0'

  spec.frameworks   = 'AssetsLibrary', 'Twitter', 'Social', 'StoreKit', 'CoreLocation', 'AdSupport', 'Accounts', 'CoreMedia', 'Security', 'SystemConfiguration', 'MobileCoreServices', 'AudioToolBox', 'AVFoundation', 'AssetsLibrary', 'CoreText', 'ImageIO', 'MediaPlayer', 'CFNetwork', 'QuartzCore', 'MessageUI', 'UIKit', 'Foundation', 'CoreGraphics', 'CoreData' 
  
  spec.requires_arc = true
  spec.homepage     = 'http://www.coscico.com'
  spec.summary      = 'A platform for learning, planning, sharing.'
  spec.author       = { 'CoSciCo Technologies' => 'info@coscico.com' }
end
