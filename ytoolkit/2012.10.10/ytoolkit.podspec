Pod::Spec.new do |s|
  s.name         = "ytoolkit"
  s.version      = "2012.10.10"
  s.summary      = "YToolkit includes a base64 lib, a handy cocoa categories lib, and an oauth 1.0 & 2.0 lib."
  s.description  = "At the moment , the YToolkit includes a base64 lib (implemented in C, with NSData/NSString catetories), a handy cocoa categories lib, an oauth 1.0 & 2.0 lib (implemented in C function but using Cocoa object arguments, with NSMutableURLRequest & ASIHTTPRequest categories)." 
  s.homepage     = "https://github.com/sprhawk/ytoolkit"
  s.license      = { :type => 'BSD' }
  s.author       = { "sprhawk" => "http://twitter.com/#!/sprhawk" }
  s.source       = { :git => "https://github.com/sunwei415/ytoolkit.git", :tag => "2012.10.10"}
  s.platform     = :ios
  s.source_files = '**/**/*.{h,m}'
  s.exclude_files = '**/*OSX*', '**/*test*'
  # s.preserve_paths = "AUTHORS.txt"
  s.dependency 'ASIHTTPRequest'
end