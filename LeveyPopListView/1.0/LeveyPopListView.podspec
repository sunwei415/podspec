Pod::Spec.new do |s|
  s.name          = 'LeveyPopListView'
  s.version       = '1.0'
  s.license       = 'MIT'
  s.requires_arc  = true
  s.summary       = 'an alternative of UIActionSheet'
  s.homepage      = 'https://bitbucket.org/sunwei415/leveypoplistview'
  s.author        = { 'waterlou' => 'https://github.com/waterlou' }

  s.platform      = :ios,'7.0'
  s.source        = { :git => 'https://bitbucket.org/sunwei415/leveypoplistview.git', :tag => 'v1.0' }
  ## s.preferred_dependency = 'Core'

  s.frameworks    = 'UIKit', 'QuartzCore'
  s.source_files  = 'LeveyPopListViewDemo/LeveyPopListView/*.{h,m}'

end
