Pod::Spec.new do |s|
  s.name     = 'ChineseToPinYin'
  s.version  = '1.0'
  s.platform = :ios, '6.0'
  s.license  = 'MIT'
  s.summary  = '汉字转拼音, 繁体与简体互相转换 代码源自网络.'
  s.homepage = 'https://github.com/sunwei415/ChineseToPinYin.git'
  s.author   = { 'Wei Sun' => 'sunwei415@126.com' }
  s.source   = { :git => 'https://github.com/sunwei415/ChineseToPinYin.git', :tag => "v1.0" }
  s.requires_arc = false
  s.description  = 'Convert Chinese Character to PinYin'

  s.source_files = 'ChineseToPinyin.{h,m}'
end
