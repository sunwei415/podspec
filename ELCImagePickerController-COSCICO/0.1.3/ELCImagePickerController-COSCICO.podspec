Pod::Spec.new do |s|
    s.name = 'ELCImagePickerController-COSCICO'
    s.version = '0.1.3'
    s.summary = 'A Multiple Selection Image Picker.'
    s.homepage = 'https://github.com/sunwei415/ELCImagePickerController'
    s.license = {
      :type => 'MIT',
      :file => 'README.md'
    }
    s.author = {'ELC Technologies' => 'http://elctech.com'}
    s.source = {:git => 'https://github.com/sunwei415/ELCImagePickerController.git',
    			:tag => '0.1.3'
    		   }
    s.platform = :ios, '5.0'
    s.resources = 'Classes/**/*.{xib,png}', 'ELCImagePickerResources.bundle'
    s.source_files = 'Classes/ELCImagePicker/*.{h,m}'
    s.framework = 'Foundation', 'UIKit', 'AssetsLibrary'
    s.requires_arc = false
end
