Pod::Spec.new do |s|
  s.name     = 'ImageMagick'
  s.version  = '6.8.0'
  s.license      = {
    :type => 'Commercial',
    :text => <<-LICENSE
              All text and design is copyright © 2010-2013 ImageMagick, Inc.

              All rights reserved.

              http://www.imagemagick.org
    LICENSE
  }
  s.summary  = 'ImageMagick® is a software suite to create, edit, compose, or convert bitmap images.'
  s.homepage = 'http://www.imagemagick.org'
  s.author   = { 'ImageMagick' => 'support@imagemagick.org' }
  s.source   = { :git => 'https://bitbucket.org/sunwei415/imagemagick.git', :tag => s.version.to_s }
  s.platform = :ios
  s.source_files = '**/*.h'
  s.preserve_paths = 'libMagickCore.a', 'libMagickWand.a', 'libjpeg.a', 'libpng.a', 'libtiff.a'
  s.library = 'MagickCore', 'MagickWand', 'jpeg', 'png', 'tiff'
  s.xcconfig  =  { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/ImageMagick"' }
end
